package com.properties_presentation

import androidx.lifecycle.SavedStateHandle
import com.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class PropertiesViewModel @Inject constructor(savedStateHandle: SavedStateHandle) :
    BaseViewModel() {

    val x = MutableStateFlow(savedStateHandle.get<Int>("destinationId"))
}