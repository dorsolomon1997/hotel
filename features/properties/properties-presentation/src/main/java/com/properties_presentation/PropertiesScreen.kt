package com.properties_presentation

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController

@Composable
fun PropertiesScreen(
    navController: NavController,
    viewModel: PropertiesViewModel = hiltViewModel()
) {
    val x by viewModel.x.collectAsState()
    Text(x.toString())
}