package com.credentials_presentation

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun CredentialsScreen(credentialsViewModel: CredentialsViewModel = hiltViewModel()) {
    val data by credentialsViewModel.data.collectAsState()
    Text(data)
}