package com.credentials_presentation

import com.credentials_domain.Test
import com.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

@HiltViewModel
class CredentialsViewModel @Inject constructor(test: Test) : BaseViewModel() {

    private val _data = MutableStateFlow(test.generateShit())
    val data = _data.asStateFlow()
}