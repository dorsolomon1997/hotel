package com.usecase

import com.domain.util.executeNetworkRequest
import com.location_domain.repo.LocationsRepo
import javax.inject.Inject

class GetLocationUseCase @Inject constructor(private val locationsRepo: LocationsRepo) {

    suspend operator fun invoke(searchQuery: String) = executeNetworkRequest {
        locationsRepo.getLocations(searchQuery)
    }
}