package com.usecase.di

import com.location_data.repo.LocationsRepoImpl
import com.location_domain.repo.LocationsRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class LocationsUseCaseModule {

    @Binds
    @Singleton
    abstract fun bindsLocationsService(LocationsRepoImpl: LocationsRepoImpl): LocationsRepo

    companion object {

    }
}