package com.usecase

import javax.inject.Inject

class LocationsInteractor @Inject constructor(val getLocationUseCase: GetLocationUseCase)