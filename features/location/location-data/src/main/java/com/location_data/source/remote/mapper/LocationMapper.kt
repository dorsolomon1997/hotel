package com.location_data.source.remote.mapper

import com.data.remote.base.BaseMapper
import com.location_data.source.remote.dto.LocationDto
import com.location_domain.domain.Entity
import com.location_domain.domain.Location
import com.location_domain.domain.Suggestion
import javax.inject.Inject

class LocationMapper @Inject constructor() : BaseMapper<LocationDto, Location> {
    override fun fromTo(from: LocationDto): Location = with(from) {
        Location(suggestions?.map { suggestionDto ->
            with(suggestionDto) {
                Suggestion(entities?.map { entityDto ->
                    with(entityDto) {
                        Entity(
                            caption.toString(),
                            destinationId.toString(),
                            geoId.toString(),
                            landmarkCityDestinationId.toString(),
                            latitude ?: 0.0,
                            longitude ?: 0.0,
                            name.toString(),
                            type.toString()
                        )
                    }
                } ?: emptyList(), group.toString())
            }

        } ?: emptyList(), term.toString())
    }
}