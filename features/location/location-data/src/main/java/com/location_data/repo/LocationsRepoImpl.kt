package com.location_data.repo

import com.location_data.source.remote.mapper.LocationMapper
import com.location_data.source.remote.service.LocationService
import com.location_domain.domain.Location
import com.location_domain.repo.LocationsRepo
import javax.inject.Inject

class LocationsRepoImpl @Inject constructor(
    private val locationService: LocationService,
    private val locationMapper: LocationMapper
) : LocationsRepo {

    override suspend fun getLocations(searchQuery: String): Location =
        locationMapper.fromTo(locationService.getLocations(searchQuery))
}