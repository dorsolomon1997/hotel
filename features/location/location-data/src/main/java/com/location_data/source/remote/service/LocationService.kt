package com.location_data.source.remote.service

import com.location_data.source.remote.LOCATION_SERVICE_POSTFIX
import com.location_data.source.remote.dto.LocationDto
import retrofit2.http.GET
import retrofit2.http.Query

interface LocationService {

    @GET(LOCATION_SERVICE_POSTFIX)
    suspend fun getLocations(@Query("query") searchQuery: String): LocationDto
}