package com.location_data.source.remote.dto


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LocationDto(
    @Json(name = "autoSuggestInstance")
    val autoSuggestInstance: Any? = Any(),
    @Json(name = "geocodeFallback")
    val geocodeFallback: Boolean? = false,
    @Json(name = "misspellingfallback")
    val misspellingfallback: Boolean? = false,
    @Json(name = "moresuggestions")
    val moresuggestions: Int? = 0,
    @Json(name = "suggestions")
    val suggestions: List<SuggestionDto>? = listOf(),
    @Json(name = "term")
    val term: String? = "",
    @Json(name = "trackingID")
    val trackingID: String? = ""
)

@JsonClass(generateAdapter = true)
data class SuggestionDto(
    @Json(name = "entities")
    val entities: List<EntityDto>? = listOf(),
    @Json(name = "group")
    val group: String? = ""
)

@JsonClass(generateAdapter = true)
data class EntityDto(
    @Json(name = "caption")
    val caption: String? = "",
    @Json(name = "destinationId")
    val destinationId: String? = "",
    @Json(name = "geoId")
    val geoId: String? = "",
    @Json(name = "landmarkCityDestinationId")
    val landmarkCityDestinationId: String? = "",
    @Json(name = "latitude")
    val latitude: Double? = 0.0,
    @Json(name = "longitude")
    val longitude: Double? = 0.0,
    @Json(name = "name")
    val name: String? = "",
    @Json(name = "redirectPage")
    val redirectPage: String? = "",
    @Json(name = "searchDetail")
    val searchDetail: Any? = Any(),
    @Json(name = "type")
    val type: String? = ""
)