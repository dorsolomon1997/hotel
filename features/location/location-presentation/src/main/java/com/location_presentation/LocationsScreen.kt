package com.location_presentation

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.common.LoadingState
import com.location_presentation.components.SearchLocationTextField
import com.presentation.components.ProgressBar
import com.presentation.navigation.Screen

@ExperimentalComposeUiApi
@Composable
fun LocationsScreen(navController: NavController, viewModel: LocationsViewModel = hiltViewModel()) {
    val scaffoldState = rememberScaffoldState()
    val locationsState by viewModel.locationsState.collectAsState()
    Scaffold(scaffoldState = scaffoldState, backgroundColor = Color.White) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            SearchLocationTextField { searchQuery ->
                viewModel.onLocationsEvent(LocationsEvents.SearchLocationsEvent(searchQuery = searchQuery))
            }
            if (locationsState.loadingState == LoadingState.LOADING)
                ProgressBar()
            locationsState.failureMsg?.let {
                Text("Error")
            }
            locationsState.location?.let { location ->
                LazyColumn(
                    contentPadding = PaddingValues(20.dp),
                    verticalArrangement = Arrangement.spacedBy(20.dp)
                ) {
                    items(location.suggestions) { suggestion ->
                        if (suggestion.entities.isNotEmpty()) {
                            Text(
                                suggestion.suggestionType,
                                color = Color.Black,
                                fontSize = 20.sp,
                                fontStyle = FontStyle.Normal,
                                fontWeight = FontWeight.Bold,
                            )
                            Spacer(modifier = Modifier.height(10.dp))
                            LazyRow {
                                items(suggestion.entities) { entity ->
                                    Column(
                                        modifier = Modifier
                                            .padding(20.dp)
                                            .clickable {
                                                navController.navigate(Screen.PropertiesScreen("destinationId" to entity.destinationId).screenRoute)
                                            },
                                        verticalArrangement = Arrangement.Center,
                                        horizontalAlignment = Alignment.CenterHorizontally
                                    ) {
                                        Text(entity.name)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalComposeUiApi
@Composable
@Preview
fun LocationsScreenPreview() {
    //LocationsScreen(navController = rememberNavController())
}