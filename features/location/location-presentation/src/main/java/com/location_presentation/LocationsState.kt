package com.location_presentation

import com.common.LoadingState
import com.location_domain.domain.Location

sealed interface LocationsState {

    class LocationsResult(
        val location: Location? = null,
        val failureMsg: String? = null,
        val loadingState: LoadingState = LoadingState.IDLE_LOADING
    ) : LocationsState
}