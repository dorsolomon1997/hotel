package com.location_presentation

sealed interface LocationsEvents {

    class SearchLocationsEvent(val searchQuery: String) : LocationsEvents
}