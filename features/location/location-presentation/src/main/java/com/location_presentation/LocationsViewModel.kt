package com.location_presentation

import androidx.lifecycle.viewModelScope
import com.common.Response
import com.presentation.base.BaseViewModel
import com.usecase.LocationsInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LocationsViewModel @Inject constructor(private val locationsInteractor: LocationsInteractor) :
    BaseViewModel() {

    private val _searchQuery = MutableStateFlow("")

    private val _searchLocations = _searchQuery.debounce(300).filter { searchQuery ->
        return@filter searchQuery.isNotEmpty()
    }.distinctUntilChanged().flatMapLatest { searchQuery ->
        locationsInteractor.getLocationUseCase(searchQuery)
    }

    private val _locationsState = MutableStateFlow(LocationsState.LocationsResult())
    val locationsState = _locationsState.asStateFlow()

    init {
        viewModelScope.launch {
            _searchLocations.collect { response ->
                _locationsState.value = when (response) {
                    is Response.Failure -> LocationsState.LocationsResult(failureMsg = response.msg)
                    is Response.Loading -> LocationsState.LocationsResult(loadingState = response.loadingState)
                    is Response.Success -> LocationsState.LocationsResult(location = response.data)
                }
            }
        }
    }

    fun onLocationsEvent(locationsEvents: LocationsEvents) {
        when (locationsEvents) {
            is LocationsEvents.SearchLocationsEvent -> onSearchLocations(locationsEvents)
        }
    }

    private fun onSearchLocations(searchLocationsEvent: LocationsEvents.SearchLocationsEvent) {
        viewModelScope.launch {
            _searchQuery.value = searchLocationsEvent.searchQuery
        }
    }
}