package com.location_domain.repo

import com.location_domain.domain.Location

interface LocationsRepo {

    suspend fun getLocations(searchQuery: String): Location
}