package com.location_domain.domain

data class Location(
    val suggestions: List<Suggestion>,
    val term: String
)

data class Suggestion(
    val entities: List<Entity>,
    private val group: String
) {
    val suggestionType = when (group) {
        "CITY_GROUP" -> "Cities"
        "HOTEL_GROUP" -> "Hotels"
        "LANDMARK_GROUP" -> "LandMarks"
        "TRANSPORT_GROUP" -> "Transportations"
        else -> "Other"
    }
}

data class Entity(
    val caption: String,
    val destinationId: String,
    val geoId: String,
    val landmarkCityDestinationId: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val type: String
)