# Project info:
1. Clean Architecture.
2. SOLD principles.
3. Multi Module project structured by feature -> Layer.
4. gradle.properties for caching, configureondemand, parallel etc.
5. Compose.
6. Moshi.
7. Coroutines, Flow, Channel, SharedFlow, StateFlow.
8. Hotel API.