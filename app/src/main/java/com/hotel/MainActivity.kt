package com.hotel

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.hotel.ui.theme.HotelTheme
import com.location_presentation.LocationsScreen
import com.presentation.base.BaseActivity
import com.presentation.navigation.Screen
import com.properties_presentation.PropertiesScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    @ExperimentalComposeUiApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HotelTheme {
                val navController = rememberNavController()
                Surface(color = MaterialTheme.colors.background) {
                    NavHost(
                        navController = navController,
                        startDestination = Screen.LocationsScreen().screenRoute
                    ) {
                        composable(Screen.LocationsScreen().screenRoute) {
                            LocationsScreen(navController)
                        }
                        composable(
                            Screen.PropertiesScreen().screenRoute, arguments = listOf(
                                navArgument("destinationId") {
                                    type = NavType.IntType
                                }
                            )
                        ) {
                            PropertiesScreen(navController = navController)
                        }
                    }
                }
            }
        }
    }
}