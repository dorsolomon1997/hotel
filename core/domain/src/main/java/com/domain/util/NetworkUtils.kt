package com.domain.util

import com.common.LoadingState
import com.common.Response
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException
import java.io.IOException

fun <T> executeNetworkRequest(isSilent: Boolean = false, operation: suspend () -> T) =
    flow<Response<T>> {
        if (!isSilent)
            emit(Response.Loading(LoadingState.LOADING))
        try {
            emit(Response.Success(operation()))
        } catch (exc: HttpException) {
            emit(Response.Failure(exc, exc.localizedMessage, exc.code()))
        } catch (exc: IOException) {
            emit(Response.Failure(exc, exc.localizedMessage))
        } catch (exc: Exception) {
            emit(Response.Failure(exc, exc.localizedMessage))
        } finally {
        }
    }.flowOn(Dispatchers.IO)