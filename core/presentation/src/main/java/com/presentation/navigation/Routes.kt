package com.presentation.navigation

enum class Routes(val route: String) {
    Locations("Locations"),
    Properties("Properties/{destinationId}")
}