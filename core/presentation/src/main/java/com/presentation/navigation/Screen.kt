package com.presentation.navigation

sealed class Screen(
    route: Routes, params:
    Array<out Pair<String, Any>>
) {

    var screenRoute = route.route
        private set

    init {
        for (param in params) {
            screenRoute = screenRoute.replace("{${param.first}}", param.second.toString())
        }
    }

    class LocationsScreen(vararg params: Pair<String, Any>) : Screen(Routes.Locations, params)

    class PropertiesScreen(vararg params: Pair<String, Any>) : Screen(Routes.Properties, params)
}