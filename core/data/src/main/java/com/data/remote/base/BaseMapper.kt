package com.data.remote.base

interface BaseMapper<From, To> {

    fun fromTo(from: From): To

    fun toFrom(to: To): From {
        TODO("Not yet implemented")
    }

    fun fromTo(from: List<From>): List<To> = from.map { fromTo(it) }

    fun toFrom(to: List<To>): List<From> = to.map { toFrom(it) }
}