package com.data.remote.di

import com.data.constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(MoshiConverterFactory.create())
            .client(OkHttpClient.Builder().addInterceptor { chain: Interceptor.Chain ->
                return@addInterceptor chain.proceed(
                    chain.request().newBuilder()
                        .addHeader("x-rapidapi-host", "hotels4.p.rapidapi.com").addHeader(
                        "x-rapidapi-key",
                        "d4235a4a4amsh9030cbdd75f4066p1e356djsne2611a28f969"
                    ).build()
                )
            }.build()).build()
}