package com.common

enum class LoadingState {
    IDLE_LOADING, LOADING
}