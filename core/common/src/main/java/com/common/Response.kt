package com.common

sealed interface Response<T> {

    class Loading<T>(val loadingState: LoadingState) : Response<T>

    class Success<T>(val data: T) : Response<T>

    class Failure<T>(val exception: Exception, val msg: String? = null, val code: Int? = null) :
        Response<T>
}